<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Your Website</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" >
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

        <link href="./css/mycss.css" rel="stylesheet">

        <script src="//code.jquery.com/jquery-3.3.1.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>


        <script src="./js/view.js"></script>

	</head>

    <body>

        <div class="container" id="header">
            <h1>Product Company</h1>
        </div>

        <div class="alert alert-success" id="success-alert" style="width: 600px; position: relative;margin-left: auto;margin-right: auto;">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Success! </strong>
            New Coverage have added to your system.
        </div>

        <div class="alert alert-success" id="success-delete" style="width: 600px; position: relative;margin-left: auto;margin-right: auto;">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Success! </strong>
            Coverage has been deleted from your system.
        </div>

        <div class="alert alert-success" id="success-update" style="width: 600px; position: relative;margin-left: auto;margin-right: auto;">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Success! </strong>
            Coverage has been updated in your system.
        </div>

        <div class="alert alert-warning" id="warning-alert" style="width: 600px; position: relative;margin-left: auto;margin-right: auto;">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Ops! </strong>
            We couldn't find it.
        </div>

        <div class="alert alert-warning" id="add-char-alert" style="width: 600px; position: relative;margin-left: auto;margin-right: auto;">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Oops! </strong>
            Name can contain only characters
        </div>

       <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document" style="z-index: 9999">
            <div class="modal-content" style="z-index: 999">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Your about to Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                 <div class="form-group">
                    <label for="exampleInputPassword1">ID</label>
                    <input type="text" class="form-control" id="Coverage_id"   readonly="readonly">
                  </div>

                  <div class="form-group">
                            <label for="exampleInputEmail1">Product Name</label>
                            <input type="text" min="3" max="25" class="form-control" id="CoverageSelect_Change" placeholder="Enter population" required>
                     </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Cost</label>
                    <input type="number" class="form-control" id="CoverageCost_change" placeholder="Enter the Cost" required>
                  </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_change">Save changes</button>
              </div>
            </div>
          </div>
        </div>

        <div id="exTab1" class="container" style="width: 600px;margin-top: 10px;">
            <ul  class="nav nav-pills">
                <li class="active">
                    <a  href="#1a" data-toggle="tab">View</a>
                </li>
                <li><a href="#2a" data-toggle="tab">Add</a>
                </li>
                <li><a href="#3a" data-toggle="tab">Search</a>
                </li>
            </ul>

            <div class="tab-content clearfix" style="margin-top: 10px;">
                <div class="tab-pane active" id="1a">
                    <h3>View all Products</h3>
                    <div class="container" style="width: 600px">

                        <div class="row">

                            <div class="table-responsive" >

                                <table class="table table-striped table-bordered" id="Main_tbl">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product NAME</th>
                                        <th>COST</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="2a">

                    <h3>Add a New Pruduct</h3>

                    <form action="" id="addForm">

                      <div class="form-group">
                            <label for="exampleInputEmail1">Product Name</label>
                            <
                              <input type="text" min="3" max="25" class="form-control" id="CoverageSelect" placeholder="Enter Product Name" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Cost</label>
                        <input type="number"   class="form-control" id="exampleInputPassword1" placeholder="Enter cost" required>
                      </div>

                        <button id="btn" class="btn btn-primary">Submit</button>
                        <div class="loader" id="addLoader"></div>
                    </form>

                </div>

                <div class="tab-pane" id="3a">
                    <h3>Search for Product</h3>
                     <form action="" id="searchForm">
                      <div class="form-group">
                        <label for="keyLbl">Product</label>
                        
                        <input type="text" class="form-control" id="CoverageSearchSelect" required>
                      </div>
                      <button id="searchBtn"  class="btn btn-primary">Submit</button>
                        <div class="loader" id="searchLoader"></div>
                    </form>

                    <div class="container" style="width: 600px; margin-top: 50px;" >

                        <div class="row">

                            <div class="table-responsive" id="search_tbl_div">

                                <table class="table" id="search_tbl">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME OF THE CITY</th>
                                        <th>POPULATION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
  
    <footer class="page-footer font-small blue">
      <?php require "temps/footer.html"; ?>
    </footer>
 
</html>