<?php 

header('Access-Controle-Allow-Origin: *'); 
header('Access-Controle-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept'); 
header('Content-Type: application/json');
header("Access-Controle-Allow-Methods: POST,GET, OPTIONS, PUT");


	try 
	{
		
		require "../db/config.php";

		$connection = new PDO ( $dsn );

		$sql = "SELECT * FROM Product";

		$statement = $connection->prepare($sql);
		$statement->execute();

		$result = $statement->fetchAll();

		$myarr = array();
		
		foreach ($result as $row) {
			$myarr[] = array("ID" => $row["ID"], "NAME" => $row["Product_Name"], "COST" => $row["Cost"]);
		}

		echo json_encode($myarr);
	}
	
	catch(PDOException $error) 
	{
		echo $sql . "<br>" . $error->getMessage();
	}

	
?>