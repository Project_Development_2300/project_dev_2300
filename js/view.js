$(document).ready(function(){

	
	$("#success-alert").hide();
	$("#warning-alert").hide();
	$("#addLoader").hide();
	$("#searchLoader").hide();
	$("#search_tbl_div").hide();
	$("#add-char-alert").hide();
	$("#success-delete").hide();
	$("#success-update").hide();

	var example_table;

	getAllCoverage();

	function getAllCoverage(){

	 example_table = $('#Main_tbl').DataTable({
	  'ajax': {
	    "type"   : "POST",
	    "url"    : './phpServices/getCoverage.php',
	    "dataSrc": ""
	  },
	  'columns': [
	    {"data" : "ID"},
	    {"data" : "NAME"},
	    {"data" : "COST"}
	  ],
	  'lengthMenu': [5, 10, 25, 50, 75, 100 ],
	  'columnDefs': [
	    {
	        targets: 2,
	        className: 'dt-body-left'
	    },
	    {
            "targets": 3,
            "data": null,
            "defaultContent": "<p data-placement='top' data-toggle='tooltip' title='Delete'><button id='del' click='deleteEntry(1)' class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' data-target='#delete'><span class='glyphicon glyphicon-trash'></span></button></p>"
        },
        {
            "targets": 4,
            "data": null,
            "defaultContent": "<p data-placement='top' data-toggle='tooltip' title='Edit'><button id='upd' data-target='#exampleModalCenter' class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' data-target='#edit' ><span class='glyphicon glyphicon-pencil'></span></button></p>"
        }
	  ]
	});
	
	example_table.ajax.reload()
	}


	$('#Main_tbl tbody').on( 'click', '#del', function () {
        var data = example_table.row( $(this).parents('tr') ).data();

        deleteEntry(data['ID'],example_table);
    });

    $('#Main_tbl tbody').on( 'click', '#upd', function () {
        var data = example_table.row( $(this).parents('tr') ).data();

        $("#Coverage_id").val(data['ID']);
        $('#CoverageSelect_Change').val(data['NAME']);
        $("#CoverageCost_change").val(data['COST']);

    });


    $('#save_change').click(function(){

    	var id = $("#Coverage_id").val();
    	var name = $('#CoverageSelect_Change').val();
       	var cost = $('#CoverageCost_change').val();

    	//$("#addLoader").show();
		$.ajax({


	        type: 'POST',
	        url: './phpServices/update.php',
	        dataType: 'json',
	        data: { 'field1': id, 'field2' : name, 'field3' : cost},
	        success: function(response) {

	        	$('#exampleModalCenter').modal('hide');
	        	
	        	 $("#success-update").fadeTo(2000, 500).slideUp(500, function(){
	           		$("#success-update").slideUp(500);
	            });
	        	 $("#addLoader").hide();
	        	example_table.ajax.reload();

	        },
	        error: function(error) {
	            console.log(error);
	             $("#addLoader").hide();
	        }

	    });


    });

    $("#addForm").submit(function(){
       
      
       var name = $('#CoverageSelect').val();
       var cost = $('#exampleInputPassword1').val();

      
   			$("#addLoader").show();
   		  	$.ajax({
	        type: 'POST',
	        url: './phpServices/addCoverage.php',
	        dataType: 'json',
	        data: { 'field1': name, 'field2' : cost},
	        success: function(response) {
	        	
	        	 $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
               		$("#success-alert").slideUp(500);
                });
	        	 $("#addLoader").hide();
	        	example_table.ajax.reload();
	        },
	        error: function(error) {
	            console.log(error);
	             $("#addLoader").hide();
	             $("#add-char-alert").fadeTo(2000, 500).slideUp(500, function(){
               		$("#add-char-alert").slideUp(500);
	        }

	    });
      
	     return false;
    });


     $("#searchForm").submit(function(){
        
       var name =  $('#CoverageSearchSelect').val();

  
   		$("#searchLoader").show();
   		search(name);

	    return false;

    });

     function search(name){

     	  	$.ajax({
	        type: 'POST',
	        url: './phpServices/searchCoverage.php',
	        dataType: 'json',
	        data: JSON.stringify({ 'field1': name}),
	        success: function(response) {
	        	//append to the table body
	        	if(response.length >0){
	        		$('#search_tbl tbody').empty();
	        		response.forEach(function(item) {
	                 	$('#search_tbl tbody').append('<tr> <td>'+item.ID+'</td> <td>'+item.NAME+'</td> <td>'+item.COST+'</td></tr>');
					});
					$("#searchLoader").hide();
					$("#search_tbl_div").show();
	        	}else{
	        		$("#warning-alert").fadeTo(2000, 500).slideUp(500, function(){
	               		$("#warning-alert").slideUp(500);
	                });
	                $("#searchLoader").hide();
	                $('#search_tbl tbody').empty();
	                $("#search_tbl_div").hide();
	        	}
	        	
	        },
	        error: function(error) {
	            console.log(error);
	            $("#searchLoader").hide();
	        }

	    });

     }


});


 function deleteEntry(id,example_table){

    	$.confirm({
	    title: 'Confirm!',
	    content: 'Are you sure?',
	    buttons: {
	        cancel: function () {
	           
	        },
	        somethingElse: {
	            text: 'delete',
	            btnClass: 'btn-blue',
	            keys: ['enter', 'shift'],
	            action: function(){
	        	
	        		$.ajax({

				        type: 'POST',
				        url: './phpServices/delete.php',
				        dataType: 'json',
				        data: JSON.stringify({ 'field1': id}),
				        success: function(response) {
				       		if(response.Status == "1"){

				       			$("#success-delete").fadeTo(2000, 500).slideUp(500, function(){
				               		$("#success-delete").slideUp(500);
				                });

				       		}
				       		
				       		example_table.ajax.reload();


				        },
				        error: function(error) {
				            console.log(error);
				           
				        }

				    });

	            }
	        }
	    }
	});
}